/*
 * Various useful C++ cross-platform functions wrapped in a venn namespace
 * Created by Jonathan Hyde, Venn Audio, 2019
 */
#pragma once

#ifdef _WIN32
#include <direct.h>
// MSDN recommends against using getcwd & chdir names
#define cwd _getcwd
#define cd _chdir
#define __popen _popen
#define __pclose _pclose
#define crDir(X) _mkdir(X)
#define NULLOUTPUT " > null"
#else
#include "unistd.h"
#include <sys/stat.h>
#define crDir(X) mkdir(X, 0755)
#define cwd getcwd
#define cd chdir
#define __popen popen
#define __pclose pclose
#define NULLOUTPUT " > /dev/null"
#endif

#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <iostream>
#include <fstream>
#include <iterator>

namespace venn
{
    // execute a command and copy its output to a string
    inline std::string exec(const char* cmd)
    {
        std::array<char, 128> buffer;
        std::string result;
        std::unique_ptr<FILE, decltype(&__pclose)> pipe(__popen(cmd, "r"), __pclose);
        if (!pipe) {
            throw std::runtime_error("popen() failed!");
        }
        while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
            result += buffer.data();
        }
        return result;
    }
    
    // attempt to change directory, or quit the program if unable
    inline void changeDirOrExit (const std::string& dir)
    {
        if (cd (dir.c_str()) == -1)
        {
            std::cout << "Couldn't change directory to " << dir << std::endl;
            exit (1);
        }
    }

    inline bool changeDir (const std::string& dir)
    {
        if (cd (dir.c_str()) == -1)
        {
            std::cout << "Couldn't change directory to " << dir << std::endl;
            return false;
        }

        return true;
    }
    
    inline bool checkStringIsEmptyVerbose (const std::string& str, const std::string& errorMsg)
    {
        if (! str.empty())
            return true;
        
        std::cout << errorMsg << std::endl;
        return false;
    }
    
    inline void exitOnEmptyString (const std::string& str, const std::string& errorMsg)
    {
        if (! checkStringIsEmptyVerbose (str, errorMsg))
            exit (1);
    }
    
    inline void stripFromString (std::string& str, char strip)
    {
        str.erase(std::remove(str.begin(), str.end(), strip), str.end());
    }
    
    inline void copyFile (const std::string& sourceFile, const std::string& destFile)
    {
        std::ifstream source (sourceFile, std::ios::binary);
        std::ofstream dest (destFile, std::ios::binary);
        
        dest << source.rdbuf();
        
        source.close();
        dest.close();
    }
    
    inline std::string getLastLine (const std::string& str)
    {
        const auto end = (int) str.size() - 1;
        int subStart = end;
        for (int i = end - 1; i >= 0; i--)
        {
            if (str[i] == '\n')
            {
                subStart = i + 1;
                break;
            }
        }
        
        return str.substr (subStart, end - subStart);
    }

    inline int tryCmd (const std::string& cmd, int& errorCounter, bool nulloutput = true)
    {
        const auto newcmd = nulloutput ? cmd + NULLOUTPUT : cmd;
        const auto result = system (newcmd.c_str());
        errorCounter += result;
        return result;
    };
}
