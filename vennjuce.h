/*
 * Useful JUCE related functions (ONLY WORKS ON JUCE PROJECTS)
 * Created by Jonathan Hyde, Venn Audio, 2019
 */
#pragma once
#include "JuceHeader.h"

namespace venn
{
    inline Result createDirIfNeeded (const String& dir)
    {
        File folder (dir);
        if (! folder.exists())
            return folder.createDirectory();
        
        return Result::ok();
    }
    
    inline bool createDirIfNeededVerbose (const String& dir, int& errorCounter)
    {
        const auto result = createDirIfNeeded (dir);
        if (result.wasOk())
            return true;
        
        std::cout << result.getErrorMessage();
        errorCounter++;
        return false;
    }

    
    inline void copyFileJuce (const std::string& source, const std::string& dest)
    {
        File sourceFile (source);
        File destFile (dest);

        if (! sourceFile.exists())
            return;

        if (! createDirIfNeeded (destFile.getParentDirectory().getFullPathName()).wasOk())
            return;

        sourceFile.copyFileTo (destFile);
    }

    inline void copyFolder (const std::string& source, const std::string& dest)
    {
        File sourceFolder (source);
        File destFolder (dest);
        
        if (! sourceFolder.exists())
            return;
        
        if (! createDirIfNeeded (destFolder.getFullPathName()).wasOk())
            return;
        
        sourceFolder.copyDirectoryTo (destFolder);
    }

    inline Result copyFolderWithResult (const std::string& source, const std::string& dest)
    {
        File sourceFolder (source);
        File destFolder (dest);

        if (! sourceFolder.exists())
            return Result::fail (source + " not found for copying");

        if (destFolder.exists())
        {
            // delete the destination folder (DANGEROUS)
            destFolder.setReadOnly (false, true);
            destFolder.deleteRecursively();
        }

        const auto result = createDirIfNeeded (destFolder.getFullPathName());
        if (! result.wasOk())
            return result;

        if (! sourceFolder.copyDirectoryTo (destFolder))
            return Result::fail ("failed to copy " + source + " into " + dest);

        return Result::ok();
    }

    inline void copyFileVerbose (const std::string& source, const std::string& dest, int& errorCounter)
    {
        File sourceFile (source);
        File destFile (dest);

        if (! sourceFile.exists())
        {
            std::cout << "Error: " << source << " not found for copying" << std::endl;
            errorCounter++;
            return;
        }

        if (destFile.exists())
        {
            // delete the destination file (DANGEROUS)
            destFile.deleteFile();
        }

        if (! createDirIfNeededVerbose (destFile.getParentDirectory().getFullPathName(), errorCounter))
            return;

        if (! sourceFile.copyFileTo (destFile))
        {
            std::cout << "Error: failed to copy " << source << " into " << dest << std::endl;
            errorCounter++;
        }
    }

    inline Result copyFileWithResult (const std::string& source, const std::string& dest)
    {
        File sourceFile (source);
        File destFile (dest);

        if (! sourceFile.exists())
            return Result::fail (source + " not found for copying");

        if (destFile.exists())
        {
            // delete the destination file (DANGEROUS)
            destFile.deleteFile();
        }

        const auto result = createDirIfNeeded (destFile.getParentDirectory().getFullPathName());
        if (! result.wasOk())
            return result;

        if (! sourceFile.copyFileTo (destFile))
            return Result::fail ("failed to copy " + source + " into " + dest);

        return Result::ok();
    }
    
    inline void copyFolderVerbose (const std::string& source, const std::string& dest, int& errorCounter)
    {
        File sourceFolder (source);
        File destFolder (dest);
        
        if (! sourceFolder.exists())
        {
            std::cout << "Error: " << source << " not found for copying" << std::endl;
            errorCounter++;
            return;
        }
        
        if (destFolder.exists())
        {
            // delete the destination folder (DANGEROUS)
            destFolder.setReadOnly (false, true);
            destFolder.deleteRecursively();
        }
        
        if (! createDirIfNeededVerbose (destFolder.getFullPathName(), errorCounter))
            return;
        
        if (! sourceFolder.copyDirectoryTo (destFolder))
        {
            std::cout << "Error: failed to copy " << source << " into " << dest << std::endl;
            errorCounter++;
        }
    }

    inline void messageManagerFinishTasks()
    {
        bool tasksPending = true;
        MessageManager::callAsync ([&tasksPending]() { tasksPending = false; });
        while (tasksPending)
            MessageManager::getInstance()->runDispatchLoopUntil (0);
    }
}
